#!/usr/bin/bash

[[ -n "${LIB_MAKEPKG_OPT_SOURCE_SH}" ]] && return
LIB_MAKEPKG_OPT_SOURCE_SH=1

#***********************************************************
declare LIBRARY=${LIBRARY:-'/usr/share/makepkg'}

#source "/usr/share/brush/util/message.sh"
source "${LIBRARY}/util/pkgbuild.sh"
source "${LIBRARY}/util/source.sh"

for lib in "$LIBRARY/source/"*.sh; do
    source "$lib"
done

declare LIB_MAKEPKG_OPT="${LIB_MAKEPKG_OPT:=/usr/share/makepkg-opt}"

for lib in "${LIB_MAKEPKG_OPT}/source/"*.sh; do
    source "${lib}"
done
#***********************************************************

###
function extract_sources_files() {
    msg "$(gettext "Extracting sources...")"
    local netfile
    local all_sources

    get_all_sources_for_arch 'all_sources'
    for netfile in "${all_sources[@]}"; do
        local file=$(get_filename "${netfile}")
        local proto=$(get_protocol "${netfile}")

        case "${proto}" in
            bzr*)
                extract_bzr "${netfile}"
                ;;
            git*)
                extract_git "${netfile}"
                ;;
            hg*)
                extract_hg "${netfile}"
                ;;
            svn*)
                extract_svn "${netfile}"
                ;;
            *)
                extract_compressed "${file}"
                ;;
        esac
    done
}
