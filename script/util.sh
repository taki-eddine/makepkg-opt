#!/bin/bash

[[ -n "${LIB_MAKEPKG_OPT_UTIL_SH}" ]] && return
LIB_MAKEPKG_OPT_UTIL_SH=1

#***********************************************************
declare LIB_MAKEPKG_OPT="${LIB_MAKEPKG_OPT:=/usr/share/pacmac}"

for lib in "${LIB_MAKEPKG_OPT}/util/"*.sh; do
    source "${lib}"
done
#***********************************************************
