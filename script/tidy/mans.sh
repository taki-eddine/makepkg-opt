#!/usr/bin/bash

[[ -n "${LIB_MAKEPKG_OPT_TIDY_MANS_SH}" ]] && return
LIB_MAKEPKG_OPT_TIDY_MANS_SH=1

#***********************************************************
declare LIBRARY=${LIBRARY:-'/usr/share/makepkg'}

#source "/usr/share/brush/util/message.sh"
source "${LIBRARY}/util/option.sh"
#***********************************************************

declare packaging_options+=('mans')
declare tidy_remove+=('tidy_mans')

###
function tidy_mans() {
    if check_option "mans" "n"; then
        msg2 "Removing mans files..."
        brush::log.d "mans.sh @ tidy_mans()" "PWD = ${PWD}"
        rm -rf "usr/share/man"
    fi
}
