#!/usr/bin/bash
#
#   emptydirs.sh - Remove empty directories from the package
#
#   Copyright (c) 2013-2016 Pacman Development Team <pacman-dev@archlinux.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[[ -n "${LIB_MAKEPKG_OPT_TIDY_REMOVEDIRS_SH}" ]] && return
LIB_MAKEPKG_OPT_TIDY_REMOVEDIRS_SH=1

#***********************************************************
source "/usr/share/makepkg/util/option.sh"

#source "/usr/share/brush/util/message.sh"
#source "/usr/share/brush/util/log.sh"
#***********************************************************

declare packaging_options+=('removedirs')
declare tidy_remove+=('tidy_removedirs')

###
function tidy_removedirs() {
    #brush::log.i "pacmac * tidy/removedirs.sh * tidy_removedirs()" "check_option 'removedirs': value=$(check_option "removedirs" "y"; echo ${?})"

    if check_option "removedirs" "y"; then
        msg2 "$(gettext "Removing empty directories...")"
        # we are unable to use '-empty' as it is non-POSIX and not support by all find variants
        find . -depth -type d -exec rmdir '{}' \; 2> /dev/null
    fi
}
