#!/usr/bin/bash
#
#   zipman.sh - Compress man and info pages
#
#   Copyright (c) 2011-2016 Pacman Development Team <pacman-dev@archlinux.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[[ -n "$LIB_MAKEPKG_OPT_TIDY_ZIPMAN_SH" ]] && return
LIB_MAKEPKG_OPT_TIDY_ZIPMAN_SH=1

#***********************************************************
LIBRARY=${LIBRARY:-'/usr/share/makepkg'}

#source "/usr/share/brush/util/message.sh"
source "${LIBRARY}/util/option.sh"
#***********************************************************

declare MAN_DIRS

declare packaging_options+=('zipman')
declare tidy_modify+=('tidy_zipman')

###
function tidy_zipman() {
    local file
    local inode
    local link
    local files=()

    if check_option "zipman" "y" && [[ -n ${MAN_DIRS[*]} ]]; then
        msg2 "$(gettext "Compressing man and info pages...")"

        while read -rd ' ' inode; do
            read file

            find ${MAN_DIRS[@]} -type l 2>/dev/null |
                while read -r link ; do
                    if [[ "${file}" -ef "${link}" ]] ; then
                        rm -f "$link" "${link}.gz"
                        if [[ ${file%/*} = ${link%/*} ]]; then
                            ln -s -- "${file##*/}.gz" "${link}.gz"
                        else
                            ln -s -- "/${file}.gz" "${link}.gz"
                        fi
                    fi
                done

            if [[ -z ${files[$inode]} ]]; then
                files[$inode]=$file
                gzip -9 -n -f "$file"
            else
                rm -f "$file"
                ln "${files[$inode]}.gz" "${file}.gz"
                chmod 644 "${file}.gz"
            fi
        done < <(find ${MAN_DIRS[@]} -type f \! -name "*.gz" \! -name "*.bz2" \
                 -exec stat -c '%i %n' '{}' + 2>/dev/null)
    fi
}
