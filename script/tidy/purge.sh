#!/usr/bin/bash
#
#   purge.sh - Remove unwanted files from the package
#
#   Copyright (c) 2008-2016 Pacman Development Team <pacman-dev@archlinux.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[[ -n "${LIB_MAKEPKG_OPT_TIDY_PURGE_SH}" ]] && return
LIB_MAKEPKG_OPT_TIDY_PURGE_SH=1

#***********************************************************
declare LIBRARY=${LIBRARY:-'/usr/share/makepkg'}

#source "/usr/share/brush/util/message.sh"
source "${LIBRARY}/util/option.sh"
#***********************************************************

declare packaging_options+=('purge')
declare tidy_remove+=('tidy_purge')

###
function tidy_purge() {
    if check_option "purge" "y" && [[ -n ${PURGE_TARGETS[*]} ]]; then
        msg2 "$(gettext "Purging unwanted files...")"
        local purge_target
        for purge_target in "${PURGE_TARGETS[@]}"; do
            if [[ ${purge_target} = "${purge_target//\/}" ]]; then
                find . ! -type d -name "${purge_target}" -exec rm -f -- '{}' +
            else
                rm -f ${purge_target}
            fi
        done
    fi
}
