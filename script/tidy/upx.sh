#!/usr/bin/bash

[[ -n "${LIB_MAKEPKG_OPT_TIDY_UPX_SH}" ]] && return
LIB_MAKEPKG_OPT_TIDY_UPX_SH=1

#***********************************************************
#source "/usr/share/brush/util/message.sh"
source "/usr/share/makepkg/util/option.sh"

#source "/usr/share/brush/util/log.sh"
#***********************************************************

declare UPXFLAGS

declare packaging_options+=('upx')
declare tidy_modify+=('tidy_upx')

###
function tidy_upx() {
    local binary

    if check_option "upx" "y"; then
        msg2 "Compressing binaries with UPX..."
        #brush::log.d "tidy_upx()" "compressing using $(nproc) thread."

        cat <(
               while read -r binary; do
                   [[ "$(file --brief --mime-type "${binary}")" == 'application/x-executable' ]] && echo "${binary}"
               done < <(find . -type f -perm -u+w)
            ) \
        | parallel -j $(nproc) upx ${UPXFLAGS[@]} "{}" &> /dev/null
    fi
}
