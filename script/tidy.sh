#!/usr/bin/bash

[[ -n "${LIB_MAKEPKG_OPT_TIDY_SH}" ]] && return
LIB_MAKEPKG_OPT_TIDY_SH=1

#***********************************************************
#source "/usr/share/brush/util/message.sh"

declare LIB_MAKEPKG_OPT=${LIB_MAKEPKG_OPT=-"/usr/share/makepkg-opt"}
for lib in "${LIB_MAKEPKG_OPT}/tidy/"*.sh; do
    source "${lib}"
done
#***********************************************************

declare pkgdir

packaging_options=()
tidy_remove=()
tidy_modify=()

readonly -a packaging_options tidy_remove tidy_modify

###
function tidy_install() {
    cd_safe "${pkgdir}"
    msg "$(gettext "Tidying install...")"

    # options that remove unwanted files
    for func in ${tidy_remove[@]}; do
        ${func}
    done

    # options that modify files
    for func in ${tidy_modify[@]}; do
        ${func}
    done
}
