#!/usr/bin/bash

[[ -n "${LIB_MAKEPKG_OPT_UTIL_UTIL_SH}" ]] && return
LIB_MAKEPKG_OPT_UTIL_UTIL_SH=1

#***********************************************************
#source "/usr/share/brush/util/log.sh"
#source "/usr/share/brush/util/message.sh"
#***********************************************************

declare pkgdir=${pkgdir}

declare CUSTOM_REPO=${CUSTOM_REPO}
declare CUSTOM_REPO_ANY=${CUSTOM_REPO_ANY}
declare CUSTOM_REPO_X86_64=${CUSTOM_REPO_X86_64}

###
# \brief Read configurations from file
# \param config_file The configuration file
###
function read_configuration() {
    local config_file="${1}"

    if [[ -f "${config_file}" ]]; then
        source "${config_file}"
    else
        error "No configurations file was set."
        exit 1
    fi

    CUSTOM_REPO_ANY="${CUSTOM_REPO}/any"
    CUSTOM_REPO_X86_64="${CUSTOM_REPO}/x86_64"
}

###
function source_safe() {
    :;
}

###
# \brief Convert string depending on pattern
###
function convert_pattern() {
    local original_pattern="${1}"
    local target_pattern="${2}"
    local value="${3}"
    sed -r "s/${original_pattern}/${target_pattern}/" <<< "${value}"
}

###
# \brief Fetches the version online following a regex pattern
###
function fetch_version() {
    local url
    local search_pattern="((?<=tags/|tags/v)[0-9.]+$)"
    local convert_pattern
    local sort_criteria=()
    local version

    local url_data
    local versions=()

    ##
    while (( ${#} )); do
        case "${1}" in
            --url=*)
                url="${1#--url=}"
                ;;
            --regex=*)
                search_pattern="${1#--regex=}"
                ;;
            --convert=*)
                convert_pattern="${1#--convert=}"
                ;;
            --sort=*)
                sort_criteria=(${1#--sort=})
                ;;
        esac
        shift
    done

    # get site data
    #brush::log.d "fetch_version()" "url = '${url}'"
    if [[ -n "${url}" ]]; then
        local cmd

        case ${url} in
            *.git )
                cmd="git ls-remote --tags"
                ;;

            * )
                cmd="wget -qO-"
        esac

        #brush::log.i "fetch_version()" "${cmd} '${url}'"
        url_data=$(${cmd} "${url}")
    else
        error "'--url' option is mandatory."
        exit 1
    fi

    # grep versions from site
    #brush::log.d "fetch_version()" "search_pattern = ${search_pattern}"
    if [[ -n "${search_pattern}" ]]; then
        versions=($(grep -oP "${search_pattern}" <<< "${url_data}" | uniq | tr '\n' ' '))
    else
        error "'--regex' option is mandatory."
        exit 1
    fi
    #brush::log.d "fetch_version()" "versions = ${versions[@]}"

    # convert the version to appropriate style
    #brush::log.d "fetch_version()" "convert_pattern = ${convert_pattern}"
    if [[ -n "${convert_pattern}" ]]; then
        versions=($(tr ' ' '\n' <<< "${versions[@]}" | sed -r ${convert_pattern} | tr '\n' ' ' ))
    fi
    #brush::log.d "fetch_version()" "versions = ${versions[@]}"

    # sort the versions
    #brush::log.d "fetch_version()" "sort_criterias = (${sort_criteria[@]})"
    versions=($(tr ' ' '\n' <<< "${versions[@]}" | sort -t'.' --version-sort ${sort_criteria[@]} | tr '\n' ' ' ))
    #brush::log.d "fetch_version()" "versions = ${versions[@]}"

    # get the latest version
    version=$(tr ' ' '\n' <<< "${versions[@]}" | tail -1)
    #brush::log.d "fetch_version()" "version = ${version}"

    echo "${version}"
}

###
function find_any_tarball() {
    local startdir="${1}"

    ls "${startdir}/"*.pkg.tar.xz 2> /dev/null
}

###
# \breif Gives pkgname from absoulte path
###
function pkgname() {
    local pkgpath="${1}"
    sed -r 's:.*/::; s:-r?[0-9].*::' <<< "${pkgpath}" 2> /dev/null
}

###
function find_package_tarball() {
    local startdir="${1}"
    local pkgname="${2}"
    ls "${startdir}/${pkgname}"-[r0-9]*-*-*.pkg.tar.xz 2> /dev/null
}


###
# \brief Create the linux directory base tree and extra directories
# \param ${@} Pattern of extra directories to create it
###
function make_base_tree_dir() {
    install -d "${pkgdir}"/etc/profile.d
    install -d "${pkgdir}"/opt/
    install -d "${pkgdir}"/usr/{bin,lib,share}
    install -d "${pkgdir}"/usr/share/{applications,icons,pixmaps,themes}

    if [[ ${#} -ne 0 ]]; then
        install -d "${@}"
    fi
}
