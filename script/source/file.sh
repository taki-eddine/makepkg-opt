#!/usr/bin/bash

[[ -n ${LIB_MAKEPKG_OPT_SOURCE_FILE_SH} ]] && return
LIB_MAKEPKG_OPT_SOURCE_FILE_SH=1

#***********************************************************
#***********************************************************

###
function extract_compressed() {
    local file="${1}"
    local filepath=$(get_filepath "${file}")

    # swap the file with a symlink.
    rm -f "${srcdir}/${file}"
    ln -s "${filepath}" "${srcdir}/"

    if in_array "${file}" "${noextract[@]}"; then
        # skip source files in the noextract=() array
        # these are marked explicitly to NOT be extracted
        return 0
    fi

    # do not rely on extension for file type.
    local file_name="${file%.*}"
    local file_type="$(file --brief --mime --uncompress --dereference "$file")"
    local ext="${file##*.}"
    local cmd=''
    local ret=0

    # start extracting.
    case "${file_type}" in
          *application/x-tar* | *application/zip* | *application/x-zip* | *application/x-cpio* \
        | *application/x-xz* | *application/x-gzip* | *application/x-bzip* )
            msg2 "$(gettext "Extracting %s with %s")" "${file}" "bsdtar"

            # TODO: optimize finding if there is a top level directory or not
            local strip_level=$([[ "$(bsdtar -tf "${file}" | egrep -o '^[^/]+/' | uniq | wc -l)" -eq 1 ]] && echo '--strip-components=1')
            # create the directory and extract inside it.
            mkdir -p "${file_name}"
            bsdtar "${strip_level}" -xkf "${file}" -C "${file_name}" || ret=$?
            ;;

        *application/vnd.debian.binary-package* )
            msg2 "$(gettext "Extracting %s with %s")" "${file}" "bsdtar"
            # create the directory and extract inside it.
            mkdir -p "${file_name}"
            bsdtar -xkf <(bsdtar -xOf "${file}" "*data.tar.*") -C "${file_name}" || ret=$?
            ;;

        *application/x-rpm* )
            msg2 "$(gettext "Extracting %s with %s")" "${file}" "bsdtar"
            mkdir -p "${file_name}"
            bsdtar -xkf "${file}" -C "${file_name}" || ret=$?
            ;;

        * )
            # ignore any unrecognizable files.
            return 0
            ;;
    esac

    if (( ret )); then
        error "$(gettext "Failed to extract %s")" "${file}"
        plain "$(gettext "Aborting...")"
        exit 1
    fi

    if (( EUID == 0 )); then
        # change perms of all source files to root user & root group
        chown -R 0:0 "${srcdir}"
    fi
}
